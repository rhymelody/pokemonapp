import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '47.86%',
    padding: '.5rem',
    margin: '.25rem',
    backgroundColor: '$primary[300]',
    borderRadius: '1rem',
  },
  iconContainer: {
    width: '30%',
    paddingHorizontal: '.65rem',
  },
  icon: {
    color: '$alternative[300]',
    fontSize: '1.75rem',
  },
  textContainer: {
    width: '70%',
  },
  text: {
    textTransform: 'capitalize',
    fontSize: '.875rem',
    fontWeight: '500',
  },
});

export default styles;
