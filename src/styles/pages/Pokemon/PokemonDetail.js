import EStyleSheet, {absoluteFill} from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '$base.primary',
    padding: '1.25rem',
    paddingTop: '.25rem',
  },
  headerActionButton: {
    backgroundColor: 'rgba(250, 250, 250, 0.675)',
    height: '2.25rem',
    width: '2.25rem',
    borderRadius: '2.25rem / 2',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerActionIcon: {
    color: '$netral[700]',
    fontSize: '1.275rem',
  },
  headerTextContainer: {
    zIndex: -5,
    elevation: -5,
    padding: '1.25rem',
    paddingTop: '.25rem',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    color: '$base.white',
    fontSize: '1rem',
    fontWeight: '400',
  },
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
  },
  container: {
    backgroundColor: '$background',
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
  },
  contentHeader: {
    alignItems: 'center',
  },
  imagePokemon: {
    aspectRatio: 1,
    width: '12rem',
    marginBottom: '.5rem',
  },
  namePokemon: {
    fontSize: '1.25rem',
    fontWeight: '600',
    textTransform: 'capitalize',
    marginBottom: '1.5rem',
  },
  actionButton: {
    paddingVertical: '.75rem',
    paddingHorizontal: '2.675rem',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '1rem',
    marginBottom: '1.5rem',
  },
  catchButton: {
    backgroundColor: '$alternative[500]',
  },
  releaseButton: {
    backgroundColor: '$danger[500]',
  },
  catchIcon: {
    fontSize: '1.375rem',
    fontWeight: '600',
    color: '$base.white',
    marginRight: '.5rem',
  },
  catchText: {
    fontSize: '1rem',
    fontWeight: '600',
    color: '$base.white',
  },
  summary: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    backgroundColor: '$alternative[100]',
    paddingHorizontal: '1.75rem',
    paddingVertical: '.75rem',
    borderRadius: '1rem',
    borderColor: '$alternative[500]',
    borderWidth: 1,
  },
  summaryContentHeader: {
    fontWeight: '700',
    marginBottom: '.275rem',
    color: '$netral[900]',
  },
  summaryContentBody: {
    fontWeight: '400',
    color: '$netral[500]',
    textTransform: 'capitalize',
  },
  contentBody: {
    paddingTop: '1.75rem',
  },
  bodySection: {
    marginBottom: '.75rem',
  },
  sectionHeader: {
    fontSize: '1.25rem',
    fontWeight: '600',
    marginBottom: '.5rem',
  },
  sectionBody: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  sectionContent: {
    backgroundColor: '$secondary[300]',
    paddingVertical: '.275rem',
    paddingHorizontal: '1rem',
    borderRadius: '.5rem',
    marginRight: '.75rem',
    marginBottom: '.5rem',
    height: '1.875rem',
    justifyContent: 'center',
    alignItems: 'center',
    text: {
      color: '$base.black',
      fontSize: '.825rem',
    },
  },
  sectionContentMore: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '$alternative[500]',
    text: {
      color: '$base.white',
      fontSize: '.825rem',
    },
    icon: {
      color: '$base.white',
      fontSize: '1rem',
      marginRight: '.325rem',
    },
  },
});

export default styles;
