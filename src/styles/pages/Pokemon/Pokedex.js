import EStyleSheet from 'react-native-extended-stylesheet';

const styles = EStyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '$base.primary',
    padding: '1.25rem',
    paddingTop: '.25rem',
  },
  headerText: {color: '$base.white', fontSize: '1rem', fontWeight: '400'},
  safeArea: {
    backgroundColor: '$background',
    flexGrow: 1,
  },
  container: {
    backgroundColor: '$background',
    padding: '1.5rem',
    width: '100%',
    flexGrow: 1,
  },
  pokemonList: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    margin: '-.25rem',
  },
  navigation: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-evenly',
    padding: '2rem',
  },
});

export default styles;
