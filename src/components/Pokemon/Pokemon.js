import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from '../../styles/components/Pokemon/Pokemon';

const Pokemon = ({id, name, url, navigation, page}) => {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() =>
        navigation.navigate(page, {id: id, name: name, url: url, page: page})
      }>
      <View style={styles.iconContainer}>
        <Icon name="pokeball" style={styles.icon} />
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.text}>{name}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Pokemon;
