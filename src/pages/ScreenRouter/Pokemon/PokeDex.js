import React, {useState, useMemo, useCallback, useEffect} from 'react';
import {View, Text, StatusBar, TouchableOpacity} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import {useQuery} from 'react-query';
import {showMessage} from 'react-native-flash-message';
import EStyleSheet from 'react-native-extended-stylesheet';
import {firebase} from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import axios from 'axios';

import styles from '../../../styles/pages/Pokemon/Pokedex';
import {Pokemon} from '../../../components/Pokemon';

const PokeDex = ({navigation}) => {
  const insets = useSafeAreaInsets();
  const [offset, setOffset] = useState(0);
  const [limit, setLimit] = useState(20);

  const {isFetching, isError, data, refetch} = useQuery(
    'pokedex-list',
    async () => {
      let url = `https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${limit}`;
      const response = await axios.get(url);
      response.data.url = url;
      return response.data;
    },
    {
      throwOnError: true,
      onError: error => {
        showMessage({
          message: error.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    },
  );

  const pokedexData = useMemo(() => {
    return data;
  }, [data]);

  const previousAction = useCallback(() => {
    offset > 0 && setOffset(value => value - limit);
  }, [offset, limit]);

  const nextAction = useCallback(() => {
    if (offset <= pokedexData.count - limit * 2) {
      setOffset(value => value + limit);
    }
  }, [offset, limit, pokedexData]);

  useEffect(() => {
    refetch();
  }, [offset, limit, refetch]);

  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={styles.header} edges={['right', 'top', 'left']}>
        <Text style={styles.headerText}>PokeDex</Text>
      </SafeAreaView>
      <SafeAreaView style={styles.safeArea} edges={['right', 'bottom', 'left']}>
        <View style={styles.container}>
          <View style={styles.pokemonList}>
            {!(isFetching || isError) &&
              pokedexData?.results.map((value, index) => (
                <Pokemon
                  key={index}
                  name={value.name}
                  url={value.url}
                  navigation={navigation}
                  page="PokeDex Detail"
                />
              ))}
          </View>
          <View style={styles.navigation}>
            <TouchableOpacity onPress={previousAction}>
              <Text>Previous</Text>
            </TouchableOpacity>
            <Text>{offset / limit + 1}</Text>
            <TouchableOpacity onPress={nextAction}>
              <Text>Next</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default PokeDex;
