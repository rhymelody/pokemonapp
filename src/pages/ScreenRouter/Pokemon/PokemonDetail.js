import React, {useState, useMemo, useCallback, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Animated,
} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Image} from '@rneui/themed';
import {useQuery} from 'react-query';
import {showMessage} from 'react-native-flash-message';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import {firebase} from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import {useBottomTabBarHeight} from '@react-navigation/bottom-tabs';

import styles from '../../../styles/pages/Pokemon/PokemonDetail';

const PokemonDetail = ({route, navigation}) => {
  const insets = useSafeAreaInsets();
  const screen = Dimensions.get('window');
  const tabBarHeight = useBottomTabBarHeight();

  const {id, name, url, page} = route.params;
  const [limitMoves, setLimitMoves] = useState(10);
  const [loadCatchPokemon, setLoadCatchPokemon] = useState(false);
  const [animBounce, setAnimBounce] = useState(new Animated.Value(-200));
  const [animFade, setAnimFade] = useState(new Animated.Value(1));

  let currentUser = auth().currentUser;
  const database = firebase
    .app()
    .database(
      'https://pokemon-rn-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  const {isFetching, isError, data} = useQuery(
    'pokedex-detail',
    async () => {
      const response = await axios.get(url);
      const getPokebag = await database
        .ref('/pokebag/' + currentUser?.uid)
        .once('value')
        .catch(error => {
          throw error;
        });

      const pokebagData = Object.values(
        Object.assign({}, getPokebag.val()),
      ).map((item, index) => {
        return {id: Object.keys(getPokebag.val())[index], ...item};
      });

      response.data.owned = pokebagData.some(
        find => find.name === response.data.name && find.url === url,
      );

      if (response.data.owned) {
        response.data.id = pokebagData.find(
          find => find.name === response.data.name && find.url === url,
        )?.id;
      }

      return response.data;
    },
    {
      throwOnError: true,
      onError: error => {
        showMessage({
          message: error.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    },
  );

  const pokemon = useMemo(() => {
    return data;
  }, [data]);

  const generateGod = useCallback(() => {
    let dataRNG = [];
    for (let i = 1; i <= 3; i++) {
      dataRNG.push(Math.floor(Math.floor(Math.random() * 20 + 5) * 20 + 1));
    }
    return dataRNG;
  }, []);

  const getGod = useCallback(() => {
    return Math.floor(Math.floor(Math.random() * 20 + 5) * 20 + 1);
  }, []);

  const pokeballAnimation = {
    transform: [{translateY: animBounce}],
    opacity: animFade,
  };

  const runAnimation = () => {
    animBounce.setValue(-200);
    animFade.setValue(1);

    Animated.loop(
      Animated.sequence([
        Animated.spring(animBounce, {
          toValue: -200,
          friction: 500,
          tension: 15,
          useNativeDriver: true,
        }),
        Animated.spring(animBounce, {
          toValue: screen.height / 2 - tabBarHeight,
          friction: 500,
          tension: 15,
          useNativeDriver: true,
        }),
        Animated.timing(animFade, {
          toValue: 0,
          duration: 500,
          useNativeDriver: true,
        }),
      ]),
    ).start(anim => {
      if (anim.finished) {
        runAnimation();
      }
    });
  };

  const catchPokemon = () => {
    const godDesicion = generateGod().some(value => value === getGod());
    setLoadCatchPokemon(value => !value);
    runAnimation();
    if (godDesicion) {
      const pushData = database.ref('/pokebag/' + currentUser?.uid).push();
      pushData
        .set({
          name: name,
          url: url,
        })
        .then(() =>
          setTimeout(() => {
            animBounce.stopAnimation();
            animFade.stopAnimation();
            setLoadCatchPokemon(value => !value);
            showMessage({
              message: 'Catch Pokemon success!',
              type: 'success',
              backgroundColor: EStyleSheet.value('$success[500]'),
              titleStyle: {textAlign: 'center'},
              floating: true,
              statusBarHeight: insets.top + 6,
            });
            navigation.navigate('PokeBag');
          }, 3000),
        )
        .catch(error =>
          showMessage({
            message: error.message,
            type: 'danger',
            backgroundColor: EStyleSheet.value('$danger[500]'),
            titleStyle: {textAlign: 'center'},
            floating: true,
            statusBarHeight: insets.top + 6,
          }),
        );
    } else {
      setTimeout(() => {
        animBounce.stopAnimation();
        animFade.stopAnimation();
        setLoadCatchPokemon(value => !value);
        showMessage({
          message: 'Pokemon run away!',
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      }, 3000);
    }
  };

  const releasePokemon = async () => {
    await database
      .ref(`/pokebag/${currentUser?.uid}/${id || pokemon.id}`)
      .remove()
      .then(() => navigation.goBack());
  };

  const toggleLimit = useCallback(() => {
    setLimitMoves(limitMoves ? undefined : 10);
  }, [limitMoves, setLimitMoves]);

  return (
    <>
      <SafeAreaView
        style={{
          backgroundColor: 'rgba(143, 149, 157, 0.5)',
          height: '100%',
          width: '100%',
          position: 'absolute',
          zIndex: loadCatchPokemon,
          elevation: loadCatchPokemon,
          alignItems: 'center',
        }}>
        <Animated.View
          style={[
            {
              width: 100,
              height: 100,
              backgroundColor: '#F2CB00',
              borderRadius: 100 / 2,
            },
            pokeballAnimation,
          ]}>
          <MaterialIcon
            name="pokeball"
            style={{
              fontSize: 100,
              color: '#3560AC',
            }}
          />
        </Animated.View>
      </SafeAreaView>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={styles.header} edges={['right', 'top', 'left']}>
        <TouchableOpacity
          testID="back_to_login_button"
          onPress={() => navigation.goBack()}>
          <View style={styles.headerActionButton}>
            <Icon name="arrow-back-outline" style={styles.headerActionIcon} />
          </View>
        </TouchableOpacity>
        <View style={[styles.headerTextContainer, {marginTop: insets.top}]}>
          <Text style={styles.headerText}>Pokemon Detail</Text>
        </View>
      </SafeAreaView>
      <SafeAreaView
        style={[styles.safeArea, {marginBottom: insets.bottom + 32}]}
        edges={['right', 'bottom', 'left']}>
        {!(isFetching || isError) && (
          <ScrollView contentContainerStyle={styles.container}>
            <View style={styles.contentHeader}>
              <Image
                containerStyle={styles.imagePokemon}
                source={{
                  uri: pokemon.sprites.other['official-artwork'].front_default,
                }}
              />
              <Text style={styles.namePokemon}>{name}</Text>
              <TouchableOpacity
                style={[
                  styles.actionButton,
                  page === 'PokeBag Detail' || pokemon.owned
                    ? styles.releaseButton
                    : styles.catchButton,
                ]}
                onPress={
                  page === 'PokeBag Detail' || pokemon.owned
                    ? releasePokemon
                    : catchPokemon
                }>
                <MaterialIcon name="pokeball" style={styles.catchIcon} />
                <Text style={styles.catchText}>
                  {page === 'PokeBag Detail' || pokemon.owned
                    ? 'Release'
                    : 'Catch'}
                </Text>
              </TouchableOpacity>
              <View style={styles.summary}>
                <View style={styles.summaryHeight}>
                  <Text style={styles.summaryContentHeader}>Height</Text>
                  <Text style={styles.summaryContentBody}>
                    {pokemon.height}
                  </Text>
                </View>
                <View style={styles.summaryWeight}>
                  <Text style={styles.summaryContentHeader}>Weight</Text>
                  <Text style={styles.summaryContentBody}>
                    {pokemon.weight}
                  </Text>
                </View>
                <View style={styles.summarySpecies}>
                  <Text style={styles.summaryContentHeader}>Species</Text>
                  <Text style={styles.summaryContentBody}>
                    {pokemon.species.name}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.contentBody}>
              <View style={styles.bodySection}>
                <Text style={styles.sectionHeader}>Type</Text>
                <View style={styles.sectionBody}>
                  {pokemon.types.map((value, index) => (
                    <View style={styles.sectionContent} key={index}>
                      <Text style={styles.sectionContent.text}>
                        {value.type.name}
                      </Text>
                    </View>
                  ))}
                </View>
              </View>
              <View style={styles.bodySection}>
                <Text style={styles.sectionHeader}>Ability</Text>
                <View style={styles.sectionBody}>
                  {pokemon.abilities.map((value, index) => (
                    <View style={styles.sectionContent} key={index}>
                      <Text style={styles.sectionContent.text}>
                        {value.ability.name}
                      </Text>
                    </View>
                  ))}
                </View>
              </View>
              <View style={styles.bodySection}>
                <Text style={styles.sectionHeader}>Moves</Text>
                <View style={styles.sectionBody}>
                  {pokemon.moves.slice(0, limitMoves).map((value, index) => (
                    <View style={styles.sectionContent} key={index}>
                      <Text style={styles.sectionContent.text}>
                        {value.move.name}
                      </Text>
                    </View>
                  ))}
                  {pokemon.moves.length > 10 && (
                    <TouchableOpacity
                      style={[styles.sectionContent, styles.sectionContentMore]}
                      onPress={toggleLimit}>
                      <MaterialIcon
                        name="pokeball"
                        style={styles.sectionContentMore.icon}
                      />
                      <Text style={styles.sectionContentMore.text}>
                        {limitMoves ? 'More' : 'Less'}
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    </>
  );
};

export default PokemonDetail;
