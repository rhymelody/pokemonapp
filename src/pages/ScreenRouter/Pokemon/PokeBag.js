import React, {useMemo, useCallback} from 'react';
import {View, Text, StatusBar} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import {useQuery} from 'react-query';
import {showMessage} from 'react-native-flash-message';
import EStyleSheet from 'react-native-extended-stylesheet';
import {firebase} from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';
import {useFocusEffect} from '@react-navigation/native';

import styles from '../../../styles/pages/Pokemon/Pokebag';
import {Pokemon} from '../../../components/Pokemon';

const PokeBag = ({navigation}) => {
  const insets = useSafeAreaInsets();

  let currentUser = auth().currentUser;
  const database = firebase
    .app()
    .database(
      'https://pokemon-rn-default-rtdb.asia-southeast1.firebasedatabase.app/',
    );

  const {isFetching, isError, data, refetch} = useQuery(
    'pokebag-list',
    async () => {
      const response = await database
        .ref('/pokebag/' + currentUser?.uid)
        .once('value');

      return Object.values(Object.assign({}, response.val())).map(
        (item, index) => {
          return {id: Object.keys(response.val())[index], ...item};
        },
      );
    },
    {
      throwOnError: true,
      onError: error => {
        showMessage({
          message: error.message,
          type: 'danger',
          backgroundColor: EStyleSheet.value('$danger[500]'),
          titleStyle: {textAlign: 'center'},
          floating: true,
          statusBarHeight: insets.top + 6,
        });
      },
    },
  );

  const pokebagData = useMemo(() => {
    return data;
  }, [data]);

  useFocusEffect(
    useCallback(() => {
      refetch();
    }, [refetch]),
  );

  return (
    <>
      <StatusBar barStyle="light-content" />
      <SafeAreaView style={styles.header} edges={['right', 'top', 'left']}>
        <Text style={styles.headerText}>PokeBag</Text>
      </SafeAreaView>
      <SafeAreaView style={styles.safeArea} edges={['right', 'bottom', 'left']}>
        <View style={styles.container}>
          <View style={styles.pokemonList}>
            {!(isFetching || isError) &&
              pokebagData?.map((value, index) => (
                <Pokemon
                  key={index}
                  id={value.id}
                  name={value.name}
                  url={value.url}
                  page="PokeBag Detail"
                  navigation={navigation}
                />
              ))}
          </View>
        </View>
      </SafeAreaView>
    </>
  );
};

export default PokeBag;
