import PokeDex from './PokeDex';
import PokeBag from './PokeBag';
import PokemonDetail from './PokemonDetail';

export {PokeDex, PokeBag, PokemonDetail};
