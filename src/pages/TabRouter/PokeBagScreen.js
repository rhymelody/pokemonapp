import React, {useCallback} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {useFocusEffect} from '@react-navigation/native';

import {PokeBag, PokemonDetail} from '../ScreenRouter/Pokemon';

const PokeBagScreen = ({navigation}) => {
  const Stack = createNativeStackNavigator();

  useFocusEffect(
    useCallback(() => {
      navigation.navigate('PokeBag List');
    }, [navigation]),
  );

  return (
    <Stack.Navigator
      initialRouteName="PokeBag List"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="PokeBag List" component={PokeBag} />
      <Stack.Screen name="PokeBag Detail" component={PokemonDetail} />
    </Stack.Navigator>
  );
};

export default PokeBagScreen;
