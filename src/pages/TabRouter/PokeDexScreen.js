import React, {useCallback} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {useFocusEffect} from '@react-navigation/native';

import {PokeDex, PokemonDetail} from '../ScreenRouter/Pokemon';

const PokeDexScreen = ({navigation}) => {
  const Stack = createNativeStackNavigator();

  useFocusEffect(
    useCallback(() => {
      navigation.navigate('PokeDex List');
    }, [navigation]),
  );

  return (
    <Stack.Navigator
      initialRouteName="PokeDex List"
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="PokeDex List" component={PokeDex} />
      <Stack.Screen name="PokeDex Detail" component={PokemonDetail} />
    </Stack.Navigator>
  );
};

export default PokeDexScreen;
