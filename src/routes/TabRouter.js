import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {Home, PokeDexScreen, PokeBagScreen} from '../pages/TabRouter';

const TabRouter = () => {
  const Tab = createBottomTabNavigator();
  const insets = useSafeAreaInsets();

  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: false,
        tabBarActiveTintColor: EStyleSheet.value("$primary['500']"),
        tabBarInactiveTintColor: EStyleSheet.value("$secondary['500']"),
        tabBarStyle: {
          height: insets.bottom + 58,
          paddingTop: 8,
          paddingBottom: insets.bottom || 8,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="home-outline" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="PokeDex"
        component={PokeDexScreen}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="pokeball" size={size} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="PokeBag"
        component={PokeBagScreen}
        options={{
          tabBarIcon: ({color, size}) => (
            <Icon name="bag-personal-outline" size={size} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabRouter;
